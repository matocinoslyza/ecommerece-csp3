import React, { useState } from 'react';
import './App.css';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Products from './pages/Products';
import Register from './pages/Register'
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import SpecificProduct from './pages/SpecificProduct';
import { Container } from 'react-bootstrap';
import { UserProvider } from './UserContext';

// For Routes
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

// The Router {BrowserRouter} component will enable us to simulate page naviation by synchronizing the shown congent nd the show URL i teh web brwoser
function App() {

  
  // React Context is nothing but a global state to the app. IT is a  way to make a prticular data available to all the componenets no matter they are nested.
  // Context helps you broadcast data and changes happening to that data/state to all components
  const [user, setUser] = useState({
    accessToken: localStorage.getItem('accessToken'),
    email: localStorage.getItem('email'),
    isAdmin: localStorage.getItem('isAdmin') === 'true',


  })
  // function for clearning localstorage on logout
  const unsetUser = () => {
    localStorage.clear()
  }

  return (
    <UserProvider value ={{user, setUser, unsetUser}}>
    <Router>
      <AppNavbar />
      <Container>
        <Routes>
        < Route path="/" element={ <Home /> }/>
          < Route path="/products" element={<Products /> }/>
            < Route path="/login" element={ <Login />} />
            < Route path="/register" element={ <Register />} />
            < Route path="/logout" element={ <Logout />} />
            < Route path="/products/:productId" element={ <SpecificProduct/> } />
            < Route path="*" element={ <Error />} />
        </Routes>
      </Container>
    </Router>
    </UserProvider>
  );
}

export default App;
