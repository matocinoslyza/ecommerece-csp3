import React, { useState, useEffect } from 'react';
import { Table } from 'react-bootstrap';

import AddProduct from './AddProduct';
import EditProduct from './EditProduct';
import ArchiveProduct from './ArchiveProduct';

export default function AdminView(props) {

	const { productsData, fetchData } = props;
	const [products, setProducts] = useState([])

	//=============Getting the productsData from the products page
	useEffect(() => {
		const productsArr = products.map(product => {
			return (
				<tr key={product._id}>
					<td>{product._id}</td>
					<td>{product.name}</td>
					<td>{product.description}</td>
					<td>{product.price}</td>
					<td className={product.isActive ? "text-success" : "text-danger"}>
						{product.isActive ? "Availabe" : "Unavailabe"}
					</td>
					<td><EditProduct product={product._id} fetchData={fetchData}/></td>
					<td><ArchiveProduct product={product._id} isActive={product.isActive} fetchData={fetchData}/></td>		
				</tr>


				)
		})

		setProducts(productsArr)

	}, [productsData])



	//=============end of useEffect for productsData


	return(
		<>
			<div className="text-center my-4">
				<h1> Admin Dashboard</h1>
				<AddProduct fetchData={fetchData}/>
			</div>

			<Table striped bordered hover responsive>
				<thead className="bg-dark text-white">
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Availability</th>
						<th colSpan="2">Actions</th>
					</tr>
				</thead>

				<tbody>
					{products}
				</tbody>
			</Table>
			
		</>

		)
}