import React from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function ArchiveProduct({product, isActive, fetchData}) {

	const archiveToggle = (productId) => {
		fetch(`https://secure-tundra-43024.herokuapp.com/products/${productId}/archive`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			}
		})

		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data === true) {
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Product successfully disabled'
				})
				fetchData();
			}else {
				Swal.fire({
					title: 'Something Went Wrong',
					icon: 'Error',
					text: 'Please Try again'
				})
				fetchData();
			}


		})
	}
 
    // reactivate
    const unarchiveToggle = (productId) => {
		fetch(`https://secure-tundra-43024.herokuapp.com/products/${productId}/reactivate`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			}
		})

		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data === false) {
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Product successfully disabled'
				})
				fetchData();
			}else {
				Swal.fire({
					title: 'Something Went Wrong',
					icon: 'Error',
					text: 'Please Try again'
				})
				fetchData();
			}


		})
	}

	return(
		<>
			{isActive ?

				<Button variant="danger" size="sm" onClick={() => archiveToggle({product})}>Archive</Button>

				:

				<Button variant="success" size="sm" onClick={() => unarchiveToggle(product)}>Unarchive</Button>

			}
		</>


		)
}
