import React from 'react';
import {Row, Col} from 'react-bootstrap';
import Button from '@mui/material/Button';

export default function Banner() {
    return(
        <Row>
            <Col className="p-5">
                <h1 className="mb-3">Aesthetics Shirts Store</h1>
                <p className="my-3">Aesthetics Shirts Store</p>
                <Button color="success" variant="contained" size="large">Buy Now!</Button>
            </Col>
        </Row>
    )
};