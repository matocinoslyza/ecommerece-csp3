import React from 'react';
import {Row, Col, Card} from 'react-bootstrap'

export default function Highlights() {
    return(
        <Row>
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Aesthetic</h2>
                        </Card.Title>
                        <Card.Text>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia,
                        molestiae quas vel sint commodi repudiandae consequuntur voluptatum laborum
                        numquam blanditiis harum quisquam eius sed odit fugiat iusto fuga praesentium
                        optio, eaque rerum!
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            
            <Col xs={12} md={4}>
            <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Vintage</h2>
                        </Card.Title>
                        <Card.Text>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia,
                        molestiae quas vel sint commodi repudiandae consequuntur voluptatum laborum
                        numquam blanditiis harum quisquam eius sed odit fugiat iusto fuga praesentium
                        optio, eaque rerum!
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            
            <Col xs={12} md={4}>
            <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Statement Tees</h2>
                        </Card.Title>
                        <Card.Text>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia,
                        molestiae quas vel sint commodi repudiandae consequuntur voluptatum laborum
                        numquam blanditiis harum quisquam eius sed odit fugiat iusto fuga praesentium
                        optio, eaque rerum! 
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    )
};