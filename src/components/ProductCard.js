import React from 'react';
import { Card } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export default function ProductCard({productProp}) {

	//Deconstruct the productProp into their own variables
	const { _id, name, description, price } = productProp;

	return(

		<Card className="m-3">
			<Card.Body>
				<Card.Title> { name } </Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text> { description } </Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>Php { price }</Card.Text>
				<Link className="btn btn-primary" to={`/products/${_id}`}>View Product</Link>


				
			</Card.Body>
		</Card>

		)
}

//Check if the ProductCard component is getting the correct prop types
//PropTypes are used for validationg information passed to a component and is a tool normally used to help developers ensure the correct information is passed from one component to the next
ProductCard.propTypes = {
	//shape() method it is used to check if a prop object conforms to a specific 'shape'
	productProp: PropTypes.shape({
		//Define the properties and their expected types
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}









