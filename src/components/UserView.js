import React, { useState, useEffect } from 'react';
import ProductCard from './ProductCard';


export default function UserView({productsList}) {

    const [products, setProducts] = useState([])

    useEffect(() => {
        const productsArr = products.map(product => {
            // only render the active products
            if(product.isActive === true) {
                return(
                    <ProductCard productProp={product} key={product._id}/>
                )
            }else{
                return null;
            }
        })
        setProducts(productsArr);
    }, [productsList])
    return(
        <>
            { products }
        </>
    )
}